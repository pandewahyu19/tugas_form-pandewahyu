<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Data Nilai</title>
  </head>
  <style>
  .container{
    background: #DCDCDC
    
  }
  </style>
  <body>
  
  

<div class="container border border-primary ">
<div class="row align-items-start">
<form action="hasil2.php" method="post">
<center>
<h2>Data Nilai Siswa</h2>
<p >Selamat Datang di Program Input Data</p>

</center>
<div class="row">
    <label for="nama" class="col-sm-2 col-form-label">NAMA</label>
    <div class="form-mb-20">
      <input type="text" class="form-control" id="nama" name="nama" >
    </div><br>
    <label for="mapel" class="col-sm-2 col-form-label">MATA PELAJARAN</label><br>
    <div class="form-mb-1">
    <select class="form-select" id="mapel" name="mapel" aria-label="Default select example">
         <option selected></option>
         <option value="Matematika">Matematika</option>
         <option value="Biologi">Bologi</option>
         <option value="Kimia">Kimia</option>
         <option value="Fisika">Fisika</option>
         <option value="Bahasa Indonesia">Bahasa Indonesia</option>
         <option value="Bahasa Inggris">Bahasa Inggris</option>
    </select>
    </div>
    <label for="nilaiuts" class="col-sm-2 col-form-label">NILAI UTS</label>
    <div class="form-mb-1">
      <input type="number" class="form-control" id="nilaiuts" min="0"max="100" name="nilaiuts" value="nilaiuts" >
    </div><br>
    <label for="nilaiuas" class="col-sm-2 col-form-label">NILAI UAS</label>
    <div class="form-mb-1">
      <input type="number" class="form-control" id="nilaiuas" min="0"max="100" name="nilaiuas" value="nilaiuas">
    </div><br>
    <label for="nilaitugas" class="col-sm-2 col-form-label">NILAI TUGAS</label>
    <div class="form-mb-1">
      <input type="number" class="form-control" id="nilaitugas" min="0"max="100" name="nilaitugas" value="nilaitugas">
    </div>
  <div class="d-grid gap-2"><br>
  <button type="submit" class="btn btn-primary">Submit</button>
  </div><br>
  <div class="d-grid gap-4">
  <button type="reset" >Reset</button>
  </div>
  </form>
</div>
</div>
 

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>
